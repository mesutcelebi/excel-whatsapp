**Excel ile Selenium Whatsapp Mesajları göndermek için**


*Chrome ve whatsapp web versiyonlarının güncel olmasına dikkat ediniz*
*Kurulum için, .NET 3.5 gereklidir* 

---

## Güncel versiyonlar
Whatsapp Sürüm: **2.2236.10**, Chrome sürümü: **106.0.5249.119**


---

## Kullanımı

Aşağıdaki adımları sırayla izleyiniz

1. Whatsapp **AÇ** butonu ile açılır
2. Telefonlar ve mesaj doldurulur.
3. Tek mesaj gönderilecekse ilk satır doldurulur


---

## Versiyonlar

**v1.0**	Çalışma şablonu oluşturuldu  
**v1.1** Selenyum açma ve mesaj gönderme butonları ayrıldı  
**v1.2** Her kişiye ayrı mesaj gönderme özelliği eklendi  
**v1.3** Kişiler arası mesaj bekleme ayarı eklendi  
**v1.4** Hangi satırda işlem yapıldığı bilgisi veriliyor. (Excel alt barı)  
**v1.5** Beklemeler yeniden düzenlendi.  
**v1.6** Whatsappın açılıp açılmadığı kontrol ediliyor.  
**v1.7** Whatsapp girişi yapılıp yapılmadığı kontrol ediliyor.  
**v1.8** Bulunamayan kişiyi bildirerek geçiyor.  
**v1.9** Kalınan yer excel bandında gösteriliyor.  
**v2.0** Mesajı,Enter vb. düzelterek gönderiyor.  
**v2.1** Tekli mesaj gönderimi açıldı.  
**v2.2** Whatsapp sürümü:Sürüm 2.2216.8, Chrome sürümü: 101.0.4951.67  
**v2.3** İzleme Barı eklenmiştir.  
**v2.4** Grafik eklendi.  
**v2.5** Whatsapp versiyon güncellemesine göre değiştirildi.  
**v2.6** Whatsapp sürümü:Sürüm 2.2218.7, Chrome sürümü: 101.0.4951.67  
**v2.7** Whatsapp sürümü:Sürüm 2.2218.8, Chrome sürümü: 102.0.5005.63  
**v2.8** Whatsapp sürümü:Sürüm 2.2224.8, Chrome sürümü: 103.0.5060.114  
**v2.9** Whatsapp sürümü:Sürüm 2.2226.4, Chrome sürümü: 103.0.5060.134  
**v3.0** Whatsapp sürümü:Sürüm 2.2228.14, Chrome sürümü: 104.0.5112.81  
**v3.1** Yetki bazlı giriş sistemi kurgulandı.  
**v3.2** Whatsapp sürümü:Sürüm 2.2236.10, Chrome sürümü: 105.0.5195.127  
**v3.3** Yetki bazlı giriş sistemi güncellendi.  
**v3.4** Rapor sayfası ayrıldı.  
**v3.5** Yinelenen numaralar siliniyor.  
**v3.6** Başlama-Bitiş eklendi.  
**v3.7** Telefon numaralarında güncelleme. Artık telefonlar 10 haneli kullanılacak. +90 sonradan ekleniyor.  
**v3.7** Telefon numaralarında güncelleme. Artık telefonlar 10 haneli kullanılacak. +90 sonradan ekleniyor.  
**v3.8** Telefon numaralarının başında 0 olup olmaması önemli değil.  
**v3.9** Telefon numaraları boşluklu ya da sayı olabiliyor.  
**v4.0** Rehbere toplu ekleme seçeneği eklendi.  
**v4.1** Whatsapp sürümü:Sürüm 2.2313.8, Chrome sürümü: 111.0.5563.111  
**v4.2** Whatsapp sürümü:Sürüm 2.2315.6, Chrome sürümü: 112.0.5615.50  